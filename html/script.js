google.charts.load('current', {'packages':['corechart']});

const app = Vue.createApp({})

app.component('bestellungen', {
    template:`

        <h2>Bestellungen</h2>

        <div class="container" style="text-align:center">
            <br>
            <br>
            <button type="button" class="btn btn-primary" @click="stopRealtimeData()" v-if="realtime">Stop getting Realtime Data</button>
            <button type="button" class="btn btn-primary" @click="getRealtimeData()" v-else>Get Realtime Data</button>
            <br>
            <br>
            <p>
                <div class="spinner-border" role="status" v-show="realtime">
                    <span class="sr-only">Loading...</span>
                </div>
            </p>

            <div id="piechart" ref="piechart" style="width: 900px; height: 500px;"></div>

        </div>

    `,
    data: function () {
        return {
            data : [],
            realtime: false,
            intervall : 0       

        }
    },
    mounted(){
        this.getData()
        google.charts.setOnLoadCallback(() => this.drawChart())  
                    
    },    
    methods:{ 
        getData(){
            axios.get(api)
                    .then(response => {
                        this.data = response.data
                        this.drawChart()
                    })
        },
        drawChart(){ 
            
            jsonData = this.data 

            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Typ');
            data.addColumn('number', 'Anzahl');

            for (var i = 0; i < jsonData.length; i++) {
                anzahl = parseInt(jsonData[i].Anzahl)
                data.addRow([jsonData[i].Typ, anzahl]);
            }

            var options = {
                title: 'Bestellte Modelle'
            };

            var chart = new google.visualization.PieChart(this.$refs.piechart);

            chart.draw(data, options);      
        },
        getRealtimeData(){
            this.realtime = true
            this.interval = setInterval(() => this.getData(), 2000);
        },
        stopRealtimeData(){
            this.realtime = false
            clearInterval(this.interval);
        }
                                

        
    },
    watch:{
    }
})

const bestellungen = app.component("bestellungen")

app.component('produktion', {
    template:`

        <h1>Produktion</h1>

        <div class="container" style="text-align:center">
            <br>
            <br>
            <button type="button" class="btn btn-primary" @click="stopRealtimeData()" v-if="realtime">Stop getting Realtime Data</button>
            <button type="button" class="btn btn-primary" @click="getRealtimeData()" v-else>Get Realtime Data</button>
            <br>
            <br>
            <p>
                <div class="spinner-border" role="status" v-show="realtime">
                    <span class="sr-only">Loading...</span>
                </div>
            </p>

            <div id="piechart" ref="piechart" style="width: 900px; height: 500px;"></div>

        </div>

    `,
    data: function () {
        return {
            data : [],
            realtime: false,
            intervall : 0       

        }
    },
    mounted(){
        this.getData()
        google.charts.setOnLoadCallback(() => this.drawChart())  
                    
    },    
    methods:{ 
        getData(){
            axios.get(api)
                    .then(response => {
                        this.data = response.data
                        this.drawChart()
                    })
        },
        drawChart(){ 
            
            jsonData = this.data 

            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Typ');
            data.addColumn('number', 'Anzahl');

            for (var i = 0; i < jsonData.length; i++) {
                anzahl = parseInt(jsonData[i].Anzahl)
                data.addRow([jsonData[i].Typ, anzahl]);
            }

            var options = {
                title: 'Bestellte Modelle'
            };

            var chart = new google.visualization.PieChart(this.$refs.piechart);

            chart.draw(data, options);      
        },
        getRealtimeData(){
            this.realtime = true
            this.interval = setInterval(() => this.getData(), 2000);
        },
        stopRealtimeData(){
            this.realtime = false
            clearInterval(this.interval);
        }
                                

        
    },
    watch:{
    }
})

const produktion = app.component("produktion")


var routes = [

    {
        path: "/bestellungen",
        components: {
            view: bestellungen
        }
    },
    {
        path: "/produktion",
        components: {
            view: produktion
        }
    },

    
   

]

const router = VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes: routes
})



app.use(router)
app.mount("#app")







